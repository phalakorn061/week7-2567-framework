const express = require("express");
const app = express();
require("dotenv").config();
const PORT = process.env.PORT || 3001;

// Get
app.get("/", (req, res) => {
  console.log("Express!");
  return res.status(200).json({ message: "Get Request to the Home Page👍" });
});
//Post
app.post("/admin", (req, res) => {
  console.log("Express!");
  return res.send("Post Request to the Admin Page👍");
});
//Put
app.put("/admin", (req, res) => {
  console.log("Express!");
  return res.send("Put Request to the Admin Page👍");
});
//Delete
app.delete("/admin", (req, res) => {
  console.log("Express!");
  return res.send("Delete Request to the Admin Page👍");
});

app.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}`);
});
